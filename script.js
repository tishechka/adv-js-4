/*
Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

Це технологія, яка дозволяє веб-сторінкам оновлюватися асинхронно, тобто без необхідності перезавантажувати всю сторінку.
AJAX є дуже корисною технологією, оскільки вона дозволяє створювати більш динамічні та інтерактивні веб-сторінки
та дозволяє веб-сторінкам завантажуватись швидше.
Це пов'язано з тим, що при використанні AJAX не потрібно завантажувати всю сторінку заново, а лише ті частини, які були змінені.
*/

function fetchData(url) {
    return fetch(url)
        .then(response => response.json())
        .catch(error => console.error('Error:', error));
}

function displayFilms(films) {
    const filmList = document.createElement('ul');
    document.body.appendChild(filmList);

    films.forEach(film => {
        const listItem = document.createElement('li');
        listItem.innerHTML = `<strong>Episode ${film.episodeId}: ${film.name}</strong><br>${film.openingCrawl}`;
        filmList.appendChild(listItem);

        fetchCharacters(film.characters, listItem);
    });
}

function fetchCharacters(characterURLs, listItem) {
    const characterPromises = characterURLs.map(url => fetchData(url));

    Promise.allSettled(characterPromises)
        .then(results => {
            const characterList = document.createElement('ul');
            listItem.appendChild(characterList);

            results.forEach(result => {
                if (result.status === 'fulfilled') {
                    const characterItem = document.createElement('li');
                    characterItem.textContent = result.value.name;
                    characterList.appendChild(characterItem);
                } else {
                    console.error('Error fetching characters:', result.reason);
                }
            });
        });
}


fetchData('https://ajax.test-danit.com/api/swapi/films')
    .then(films => displayFilms(films))
    .catch(error => console.error('Error fetching films:', error));